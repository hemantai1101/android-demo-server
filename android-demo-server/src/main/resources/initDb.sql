select item0_.id as id1_0_, item0_.description as descript2_0_, item0_.name as name3_0_ from item item0_create table item (
  id          integer not null auto_increment,
  description varchar(255),
  name        varchar(255),
  primary key (id)
);

insert into item (description, name)
values ('Description 1', 'Item 1'),
       ('Description 2', 'Item 2'),
       ('Description 3', 'Item 3');