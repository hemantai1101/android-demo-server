package com.qualicom.androiddemoserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class AndroidDemoServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(AndroidDemoServerApplication.class, args);
    }
}
