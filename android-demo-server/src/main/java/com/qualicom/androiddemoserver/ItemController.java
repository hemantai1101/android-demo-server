package com.qualicom.androiddemoserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class ItemController {

    ItemRepository itemRepository;

    @RequestMapping("/items/{itemId}")
    public ItemVo greeting(@PathVariable(value = "itemId") Integer itemId) throws Exception {
        // Simulate some loading
        Thread.sleep(500);

        Optional<Item> item = itemRepository.findById(itemId.longValue());

        if (!item.isPresent())
            throw new ItemNotFoundException("itemIds-" + itemId);

        return ItemTransformer.toVo(item.get());
    }

    @RequestMapping("/items")
    public List<ItemVo> greeting() throws Exception {
        // Simulate some loading
        Thread.sleep(1000);

        return itemRepository
                .findAll()
                .stream()
                .map(ItemTransformer::toVo)
                .collect(Collectors.toList());
    }

    @Autowired
    public void setItemRepository(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }
}
