package com.qualicom.androiddemoserver;

public class ItemTransformer {
    public static ItemVo toVo(Item item) {
        ItemVo itemVo = new ItemVo();
        itemVo.setId(item.getId());
        itemVo.setName(item.getName());
        itemVo.setDescription(item.getDescription());
        return itemVo;
    }
}
